# Creer un repertoire courant /home/kathara/curchallenge
if [ -d "/home/kathara/curchallenge" ];
then
 rm -rf "/home/kathara/curchallenge";
fi

mkdir "/home/kathara/curchallenge";
# Ajouter un lien de du git sharing vers curdir sharing /home/kathara/curchallenge
ln -s /home/kathara/katharachallenges/$(cat /home/kathara/katharachallenges/curchallenge.txt)/shared /home/kathara/curchallenge/sharing

#lxterminal --working-directory="/home/kathara/curchallenge"
