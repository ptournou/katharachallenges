# katharachallenges

Labs for Kathara Challenges.
Use Moodle for collect students' answers.
Use latex and moodle.sty to generate the moodle xml quiz

## Overall principles

The git contains several challenges listed in the challenge directory.
The curchallenge.txt file indicate the current challenge to be loaded on the VM (TODO: this has to be improved to allow multiple challenges to be availables to the students)
The VM contains the startkathara.sh script which is executed on LXSESSION startup (see the autostart file for example). The startkathara.sh clone this GIT and launch the current lab to the user. The behaviour of the startkatjara.sh script can be refined in a challenge specific script which is called after launchin Kathara (for example, see challenges/netadmindebug/startkathara2.sh which only exposes a couple of xterm from the topology, and opens a terminal containing the shared directory).

The git directory isn't deleted after launching the lab. As a consequence, motivated students can access to the answers. TODO this has to be improved.

The students has to follow the question from a moodle test. This test is imported from a challenges/netadmindebug/challenge1-moodle.xml file which is generated from challenge1.tex latex file through the command :

lualatex -shell-escape  challenge1.tex

The documentation of the moodle.sty latex package is available here : http://mirrors.ctan.org/macros/latex/contrib/moodle/moodle.pdf


## Net admin debug Challenge

Machines are deployed on diateam but can be deployed anywhere. The only things that matters are the startkathara.sh script as well as some kind of autostart script to execute the startkathara.sh script on stratup.

Of course, the VM needs to have a working Kathara installation and internet access.

## Getting started
